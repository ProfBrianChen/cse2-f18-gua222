/*
Name: Gustavo Adame Delarosa
Date: 9/3/18
Course: CSE 002-110 
About: CSE 2 Welcome Class homework. prints out Welcome, Lehigh id
*/
public class WelcomeClass {
  //main method required for every Java Program
  public static void main(String args[]) {
    //Prints Welcome and Lehigh network id to the terminal window. 
    System.out.println(" -----------"); //prints top line
    System.out.println("| WELCOME |"); //prints welcome
    System.out.println(" -----------"); //prints third line
    System.out.println("  ^  ^  ^  ^  ^  ^"); //prints fourth line
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / "); //prints fifth line
    System.out.println("<-G--U--A--2--2--2->"); //prints my Lehigh Id
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / "); //prints seventh line
    System.out.println("  v  v  v  v  v  v"); //prints eighth line
  }//end of main method
} //end of class
  


   