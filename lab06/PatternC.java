/*
Name: Gustavo Adame Delarosa
Course: CSE 002
Instructor: Professor Carr
Date: 10/13/18
Objective: The purpose of this lab is to teach you nested loops and 
patterns that will help you understand how to set up nested loops.
*/
import java.util.Scanner; //imports java class
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public class PatternC {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please input number between 1-10");
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
        while (!userInput.hasNextInt()) { //start of while loop that ask for integer when input is a string
            System.out.println("Try again, only numbers from 1-10");
            userInput.next();
        }
       int row = userInput.nextInt(); //int row stores userinput
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++           
      while (row > 10 || row < 1 ) { //start of while loop that ask for integer when input is out of range
            System.out.println("Try again, only numbers from 1-10");
            row =userInput.nextInt();
        }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++       
      for (int i = 1; i <= row; i++) {  //for loop that creates the number of rows
            for (int j = row; j > i; j--) { //for loop that creates space
                System.out.print(" ");
            }
            for (int k = i; k >= 1; k--) { //the for loop that output the number
                System.out.print(k);
            }
            System.out.println();
        }
    }//end of main method 

}//end of class